from filter import Wordfilter
from transformers import BertForSequenceClassification
from torch.utils.data import TensorDataset
from transformers import BertTokenizer
from transformers import pipeline
import tensorflow as tf
import torch
import pandas as pd
from tqdm.notebook import tqdm
pd.options.mode.chained_assignment = None


def hatespeech(article):

    wordfilter = Wordfilter()
    if wordfilter.blacklisted(article):
        return 1
    else:
        #device_name = tf.test.gpu_device_name()
        # if device_name != '/device:GPU:0':
        #    raise SystemError('GPU device not found')

        #print('Found GPU at: {}'.format(device_name))

        tokenizer = BertTokenizer.from_pretrained('bert-base-uncased', do_lower_case=True)

        device = torch.device('cpu')

        model = BertForSequenceClassification.from_pretrained(
            './bert-model-hate-speech'
        )

        Pipeline = pipeline('sentiment-analysis', model=model, tokenizer=tokenizer)

        text_to_predict = article

        return(0 if Pipeline.__call__(text_to_predict)[0]['label'] == 'LABEL_0' else 1)
